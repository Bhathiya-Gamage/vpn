# Vpn


## Installation

`apt update && apt upgrade -y && reboot`

`git clone https://gitlab.com/Bhathiya-Gamage/vpn && cd vpn && chmod +x * && ./install.sh`

### ★ Default Ports & Doors
_- Openssh Port = 22_<br>
_- Dropbear port   =  80 | 110_<br>
_- SSL + Dropbear Port  =  443_<br>
_- SSL + Openssh Port  =  444_<br>
_- Squid Proxy Port  =  8080 (squid වැඩ කරන්න පෙලෝඩ් එකේ connect request එකක් තියෙන්න ඕනේ)_<br>
_- UDPGW Port  =  7300_<br>
_- Vless ws Port  =  4443

<a href="https://gitlab.com/Bhathiya-Gamage/vpn"><img src="https://i.ibb.co/qpvCqQR/Screenshot-4.png" alt="Screenshot-1" border="0"></a>

## Authors and acknowledgment
SSH And Vray Script Credits to the Rezoth team

## License
For open source projects, say how it is licensed.

